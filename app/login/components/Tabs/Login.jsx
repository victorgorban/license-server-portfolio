//* секция Библиотеки c функциями
import React from 'react';
import _ from 'lodash';
import delay from 'delay';
import { getCookies, getCookie, setCookie, removeCookies } from 'cookies-next';
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import * as commonHelpers from '@commonHelpers';
import * as clientHelpers from '@clientHelpers';
import * as notifications from '@clientHelpers/notifications';
//* endof  Наши хелперы

//* секция Контекст и store
import { $userData } from '@clientHelpers/stores'
//* endof  Контекст и store

//* секция Компоненты из библиотек
import { Controller, useForm } from 'react-hook-form';
import { useRouter, usePathname } from 'next/navigation';
//* endof  Компоненты из библиотек

//* секция Наши компоненты
import * as formComponents from '@components/forms'
import * as animatedFormComponents from '@components/animated/forms'
import AnimatedFormBlock from '@root/components/animated/FormBlock'
//* endof  Наши компоненты

export default function Component() {
    //* секция глобальное состояние из context и store

    //* endof глобальное состояние из context и store

    //* секция состояние


    const router = useRouter();
    const pathname = usePathname();
    const [isSubmitting, setSubmitting] = React.useState(false);
    const [current_formState, setCurrent_formState] = React.useState({});
    const [last_formState, setLast_formState] = React.useState({});

    const {
        control,
        handleSubmit,
        reset,
        getValues,
        setError,
        clearErrors,
    } = useForm({
        mode: 'onTouched'
    });
    //* endof состояние

    //* секция вычисляемые переменные, изменение состояния
    //* endof вычисляемые переменные, изменение состояния

    //* секция эффекты

    //* endof эффекты

    //* секция функции-хелперы, НЕ ОБРАБОТЧИКИ
    //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ

    //* секция обработчики

    async function submitForm(data, e) {
        let objectToSend = _.cloneDeep(current_formState);

        try {
            setSubmitting(true);
            let { user: loggedUser } = await clientHelpers.submitObject(
                '/api/users/login',
                { ...objectToSend },
            );

            notifications.showSuccess('Вы вошли в систему.')
            if (loggedUser.role == 'globalAdmin') {
                router.push('/users')
            } else { router.push('/licenseKeys') }
            $userData.api.replace(loggedUser);
        } catch (e) {
            notifications.showSystemError(e.message);
            console.error(e);
        } finally {
            setSubmitting(false);
        }
    }

    function handleFormErrors(errors, e) {
        notifications.showUserError('Сначала исправьте ошибки');
    }
    //* endof обработчики

    return (
        <div className="">
            <form noValidate className="form d-flex flex-column">
                {(() => {
                    let fieldName = 'login'
                    let isDisabledField = false;
                    return <>
                        <Controller
                            control={control}
                            name={fieldName}
                            rules={{
                                required: 'Пожалуйста введите данные для входа',
                                validate(v) {

                                    return true;
                                }
                            }}
                            render={({ field, fieldState: { invalid, isTouched, isDirty, error }, formState }) => (
                                <AnimatedFormBlock htmlElement="label" className={`form-group bg-gray5 w-md-100 ${invalid ? 'error' : ''}`}>
                                    <span className="label weight-600 size-14">Имя пользователя *</span>

                                    <div className="input-wrapper">
                                        <formComponents.TextInput
                                            {...field}
                                            value={field.value || ''}
                                            onChange={(e) => {
                                                field.onChange(e);
                                                clientHelpers.handleChangeItem(e, current_formState, fieldName);
                                            }}
                                            type="text"
                                            maxLength={500}
                                            className="form-control"
                                            placeholder=""
                                        />
                                    </div>
                                    <span className="field-error">{error?.message}</span>
                                </AnimatedFormBlock>
                            )}
                        />
                    </>
                })()}

                {(() => {
                    let fieldName = 'password'
                    let isDisabledField = false;
                    return <>
                        <Controller
                            control={control}
                            name={fieldName}
                            rules={{
                                required: 'Пожалуйста введите пароль',
                                validate(v) {
                                    if (v.length < 3) {
                                        return 'Пароль должен содержать минимум 3 символа';
                                    }
                                    return true;
                                }
                            }}
                            render={({ field, fieldState: { invalid, isTouched, isDirty, error }, formState }) => (
                                <AnimatedFormBlock htmlElement="label" className={`form-group bg-gray5 w-md-100 ${invalid ? 'error' : ''}`}>
                                    <span className="label weight-600 size-14">Пароль *</span>

                                    <div className="input-wrapper">
                                        <formComponents.PasswordInput
                                            {...field}
                                            value={field.value || ''}
                                            placeholder="Ваш пароль"
                                            onChange={(e) => {
                                                field.onChange(e);
                                                clientHelpers.handleChangeItem(e, current_formState, fieldName);
                                            }}
                                        />
                                    </div>

                                    <span className="field-error">{error?.message}</span>
                                </AnimatedFormBlock>
                            )}
                        />
                    </>
                })()}

                <div className="buttons d-flex flex-wrap">
                    <div className="wrapper w-100 d-flex justify-content-center">
                        <div
                            className={`btn style-default w-100 ${isSubmitting ? 'loading' : ''} mt-05`}
                            disabled={isSubmitting}
                            onClick={handleSubmit(submitForm, handleFormErrors)}>
                            Войти
                            <div className="loader lds-ring color-brand1">
                                <div></div>
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    </div>

                    {/* <div className="wrapper w-50 d-flex justify-content-center">
                        <div className="link h-100 w-100 h-100 d-flex all-center">
                            Забыли пароль?
                        </div>
                    </div> */}
                </div>
            </form>

        </div>
    );
}
