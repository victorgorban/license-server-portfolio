//* секция Библиотеки c функциями
import { cookies as nextCookies } from 'next/headers';
import _ from "lodash";
import SimpleSchema, { Integer, Any, oneOf } from "simpl-schema";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import middlewares from './middlewares'
import * as serverHelpers from "@serverHelpers";
import * as commonHelpers from "@commonHelpers";
import * as apiResponses from "@serverHelpers/responses";
import * as Collections from "@src/ORM/Collections";
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
//* endof  Наши компоненты

export let resultSchema = {};

export let dataSchema = new SimpleSchema(
  {
  },
  serverHelpers.simpleSchemaOptions
);

/**
 * 
 */
export async function POST(req, { params }) {
  try {
    await middlewares(req);

    let cookies = nextCookies();
    let user_id = cookies.get('user_id')?.value
    let token = cookies.get('token')?.value

    let user = await Collections.users.findOne({ _id: user_id, loginTokens: token })

    let result = user ? _.pick(user, ['_id', 'name', 'username', 'phone', 'role']) : null

    return apiResponses.success(null, { user: result });
  } catch (e) {
    console.error(e);
    return apiResponses.error(e.message || e);
  }
}