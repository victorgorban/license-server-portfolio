//* секция Библиотеки c функциями
import { cookies as nextCookies } from 'next/headers';
import uniqid from "uniqid";
import SimpleSchema, { Integer, Any, oneOf } from "simpl-schema";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import middlewares from './middlewares'
import * as serverHelpers from "@serverHelpers";
import * as commonHelpers from "@commonHelpers";
import * as apiResponses from "@serverHelpers/responses";
import * as Collections from "@src/ORM/Collections";
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
//* endof  Наши компоненты

export let resultSchema = {};

export let dataSchema = new SimpleSchema(
  {
  },
  serverHelpers.simpleSchemaOptions
);

/**
 * 
 */
export async function POST(req, { params }) {
  try {
    await middlewares(req);

    let cookies = nextCookies();
    let user_id = cookies.get('user_id')?.value
    let token = cookies.get('token')?.value

    await Collections.users.updateOne(
      {
        _id: user_id,
      },
      {
        $pull: {
          loginTokens: token,
        },
      }
    );

    cookies.delete('user_id')
    cookies.delete('token')

    return apiResponses.success(null, {});
  } catch (e) {
    console.error(e);
    return apiResponses.error(e.message || e);
  }
}