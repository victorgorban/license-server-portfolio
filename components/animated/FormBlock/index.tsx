//* секция Библиотеки c функциями
import React from "react";
import { motion, useAnimation } from "framer-motion";
// import { useIsFirstRender } from "@uidotdev/usehooks";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
//* endof  Наши компоненты

// использование: useRef<ImperativeProps> ();
export type ComponentApi = {
  externalSetStayActive: (isActive: boolean) => void;
};

export type ComponentProps = {
  /** Насколько сильно увеличивать или уменьшать этот компонент формы при взаимодействии с ним */
  scalingMultiplier?: number;
  /** Выставляемый z-index для компонента при наведении или при открытом меню
   * (например, в случае работы в формах с анимированным select нужно будет использовать blockRef.externalSetStayActive.
   * Примеры можно найти в коде форм, в документации это так не покажешь).
   * Нужен для того, чтобы находиться над другими компонентами с нестандартным z-index */
  activeZIndex?: number;
  /** Выставляемый z-index для компонента по умолчанию (с закрытым меню) */
  inactiveZIndex?: number;
  /** Классы, применяемые к компоненту */
  className?: string;
  /** Стиль, применяемый к компоненту */
  style?: Record<string, any>;
  /** Название html-элемента, который будет здесь использоваться для рендера */
  htmlElement?: string;
  /** Содержимое блока */
  children: React.ReactNode;
  /** Другие свойства html-элемента */
  [key: string]: any;
};

const FormBlock = React.forwardRef<ComponentApi, ComponentProps>(
  function Component(
    {
      scalingMultiplier = 1,
      activeZIndex = 2,
      inactiveZIndex = 1,
      className,
      htmlElement = "label",
      style,
      children,
      ...otherProps
    }: ComponentProps,
    elRef
  ) {
    //* библиотеки и неизменяемые значения
    const MotionComponent = motion[htmlElement];
    const defaultState = {
      scale: 1,
      zIndex: inactiveZIndex,
      boxShadow: "0 0 0 transparent",
    };
    const activeState = {
      scale: 1 + 0.02 * scalingMultiplier,
      zIndex: activeZIndex,
      boxShadow: "0 7px 10px rgba(0, 0, 0, 0.15)",
    };
    const pressedState = { scale: 1 - 0.02 * scalingMultiplier };
    //* endof библиотеки и неизменяемые значения

    //* контекст
    //* endof контекст

    //* состояние
    const [mustStayActive, setStayActive] = React.useState(false);
    const lastRegisteredElementMouseActionTime = React.useRef(0);
    const animationControls = useAnimation();

    //* endof состояние

    //* секция вычисляемые переменные, изменение состояния

    //* endof вычисляемые переменные, изменение состояния

    //* эффекты
    React.useImperativeHandle(elRef, () => ({
      externalSetStayActive(isActive: boolean) {
        // console.log("externalSetStayActive called", isActive);
        setStayActive(isActive);
      },
    }));

    React.useEffect(() => {
      if (mustStayActive) {
        animationControls.start(activeState);
      } else {
        // сложнообъяснимая логика браузера. Т.к. наведение на открытые select-ы (и другие элементы) 
        // тоже считаются наведением на родительский элемент, то приходится идти на хитрость с таймингами. 
        // Без setTimeout это уже работать не будет.
        // setTimeout(0) и current < 0 тоже работать не будет. 
        // Этот тайминг должен быть больше разницы между 2-мя кадрами браузера (для 60fps это 16.6666667мс). 
        // Т.к. не везде это 60fps, я взял 20 кадров - меньше не упадет. 
        // Если вы уверены что у вас будет 120fps, можете поставить таймаут 20 на свой страх и риск. 
        // Но лучше ставить число > 2*(время между 2мя кадрами браузера), так будет стабильнее.
        // тем более что с таймаутом 40-50 ничего плохого нет. 
        // Визуально select закрывается, а уже в следующем кадре начинается анимация закрытия FormBlock.
        // Для сравнения, без данной фишки (например, для FormBlock c TextInput) эти события начинаются в одном кадре, 
        // а здесь (для FormBlock+Select) в соседних кадрах, вот и вся разница.
        setTimeout(() => {
          const isHopefullyHovered =
            new Date().getTime() -
              lastRegisteredElementMouseActionTime.current <
            60;
          // console.log(
          //   "isHopefullyHovered",
          //   isHopefullyHovered,
          //   "must close",
          //   !isHopefullyHovered,
          //   new Date().getTime() - lastRegisteredElementMouseActionTime.current
          // );
          // если кликнули вне блока, то блок нужно закрыть (возможно, stop). Если внутри, то закрывать не надо: блок закроется при выходе мышки за пределы.
          if (!isHopefullyHovered) {
            animationControls.start(defaultState);
          }
        }, 50);
      }
    }, [mustStayActive]);

    //* endof эффекты

    //* секция функции-хелперы, НЕ ОБРАБОТЧИКИ

    //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ

    //* обработчики
    function handleMouseAction() {
      lastRegisteredElementMouseActionTime.current = new Date().getTime();
      // console.log(
      //   "in handleMouseAction",
      //   lastRegisteredElementMouseActionTime.current
      // );
    }
    //* endof обработчики

    return (
      <>
        {/* TODO scss => StyledComponents. Нужно перенести локальные классы из scss в сам компонент, чтобы я мог настраивать свойства класса, исходя из аргументов компонента. */}
        <MotionComponent
          animate={animationControls}
          init={defaultState}
          // whileHover={activeState}
          onHoverStart={() => animationControls.start(activeState)}
          onHoverEnd={() => animationControls.start(defaultState)}
          onMouseDown={(e) => {
            handleMouseAction();
            otherProps.onMouseDown?.(e);
          }}
          onMouseUp={(e) => {
            handleMouseAction();
            otherProps.onMouseUp?.(e);
          }}
          onMouseMove={(e) => {
            handleMouseAction();
            otherProps.onMouseMove?.(e);
          }}
          whileFocus={activeState}
          // whileTap={pressedState}
          transition={{ duration: 0 }}
          className={`form-block ${
            mustStayActive && "active"
          } scalingMultiplier-${scalingMultiplier} activeZIndex-${activeZIndex} inactiveZIndex-${inactiveZIndex} ${className}`}
          style={style}
          {...otherProps}
        >
          {children}
        </MotionComponent>
      </>
    );
  }
) as React.ForwardRefExoticComponent<
  ComponentProps & React.RefAttributes<ComponentApi>
>;

export default FormBlock;
