import type { Meta, StoryObj } from "@storybook/react";
import * as React from 'react'

import Component, { ComponentProps, ComponentApi } from "./index";
import * as formComponents from "@components/forms";
import * as animatedFormComponents from "@components/animated/forms";

const meta = {
  component: Component,
  title: "components/animated/FormBlock",
  argTypes: {
    children: {
      table: {
        disable: true,
      },
    },
  },
  // TODO сделать ref-ы onMenuOpen как в формах, и проставить action-ы, раз нельзя дать варианты обработчиков. Блин, мне всего лишь нужно видеть название обработчика и параметры, с которыми он вызвался...
  parameters: {},
  decorators: [
    (Story: any) => (
      <div className="content form d-flex flex-wrap pt-30 pb-40">
        <Story />
      </div>
    ),
  ],
} satisfies Meta<typeof Component>;

export default meta;
type Story = StoryObj<typeof meta>;

const optionsForSelect = [
  { value: 1, label: "Один" },
  { value: 2, label: "Два" },
];

const selectedOption = optionsForSelect[0];

export const Primary: Story = {
  args: {
    className: "form-group w-100",
    style: { minWidth: 300 },
    children: (
      <>
        <span className="label weight-600">Название *</span>

        <div className="input-wrapper">
          <formComponents.TextInput value={"Пример текста"} placeholder="" />
        </div>
      </>
    ),
  } as ComponentProps,
};

export const AnimatedFormField: Story = {
  args: { ...Primary.args },
  parameters: {
    docs: {
      description: {
        story: "Анимированное поле внутри анимированного блока",
      },
    },
  },
  render({ ...args }) {
    return (
      <Component {...args}>
        <span className="label weight-600">Название *</span>
        <div className="input-wrapper">
          <animatedFormComponents.TextInput
            value={"Пример текста"}
            placeholder=""
          />
        </div>
      </Component>
    );
  },
};

export const FormSelect_StayActive: Story = {
  args: { ...Primary.args },
  parameters: {
    docs: {
      description: {
        story:
          "Select тоже работает внутри анимированного блока. Параметры настроены так, чтобы всё работало без дополнительной настройки. Здесь показан вариант использования, когда при открытом меню у select-а, сам компонент остается в активном состоянии",
      },
    },
  },
  render({ ...args }) {
    const formBlockRef = React.createRef<ComponentApi>();

    return (
      <Component ref={formBlockRef} {...args}>
        <span className="label weight-600">Выбор *</span>
          <div className="input-wrapper">
            <formComponents.SelectInput onMenuOpenOrClosed={(isOpen) => formBlockRef.current?.externalSetStayActive(isOpen)} value={selectedOption} defaultOptions={optionsForSelect}  />
          </div>
      </Component>
    );
  },
};


export const AnimatedFormSelect_StayActive: Story = {
  args: { ...Primary.args },
  parameters: {
    docs: {
      description: {
        story:
          "Анимированный select тоже работает внутри анимированного блока. Параметры настроены так, чтобы всё работало без дополнительной настройки. Здесь показан вариант использования, когда при открытом меню у select-а, сам компонент остается в активном состоянии",
      },
    },
  },
  render({ ...args }) {
    const formBlockRef = React.createRef<ComponentApi>();

    return (
      <Component ref={formBlockRef} {...args}>
        <span className="label weight-600">Выбор *</span>
          <div className="input-wrapper">
            <animatedFormComponents.SelectInput onMenuOpenOrClosed={(isOpen) => formBlockRef.current.externalSetStayActive(isOpen)} value={selectedOption} defaultOptions={optionsForSelect}  />
          </div>
      </Component>
    );
  },
};

export const Scaled: Story = {
  parameters: {
    docs: {
      description: {
        story: "При наведении и при фокусе увеличивается сильнее.",
      },
    },
  },
  args: {
    ...Primary.args,
    scalingMultiplier: 5,
  } as ComponentProps,
};

export const HtmlElement: Story = {
  parameters: {
    docs: {
      description: {
        story:
          "Можно выбрать html элемент, который будет рендериться. Примеры: div, article, button... Без ограничений",
      },
    },
  },
  args: {
    ...Primary.args,
    htmlElement: "article",
  } as ComponentProps,
};

export const AppearanceCustomizing: Story = {
  parameters: {
    docs: {
      description: {
        story:
          "Внешний вид можно дополнительно настроить параметрами className и style",
      },
    },
  },
  args: {
    ...Primary.args,
    className: "form-group w-100 bg-gray2",
    style: { height: 200 },
  } as ComponentProps,
};
