//* секция Библиотеки c функциями
import React from "react";
import { motion } from "framer-motion";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
//* endof  Наши компоненты
export type ComponentProps = {
  /** Значение поля */
  value?: string;
  /** Дополнительные классы */
  className?: string;
  /** Насколько сильно увеличивать или уменьшать этот компонент формы при взаимодействии с ним */
  scalingMultiplier?: number;
  /** Отображать ли вводимые символы по умолчанию */
  showPasswordByDefault?: boolean;
  /** Дополнительные классы для обертки, которая необходима из-за наличия кнопок 'show' и 'hide' */
  wrapperClassName?: string;
  /** Другие свойства html-элемента */
  [key: string]: any;
};

/**
 * Анимированный компонент для форм
 * @returns TSX Компонент
 */
export default function PasswordInput({
  value,
  className,
  scalingMultiplier = 1,
  showPasswordByDefault = false,
  wrapperClassName="",
  ...otherProps
}: ComponentProps): React.JSX.Element {
  //* секция глобальное состояние из context
  //* endof глобальное состояние из context

  //* секция состояние

  const [showPassword, setShowPassword] = React.useState(showPasswordByDefault);

  //* endof состояние

  //* секция вычисляемые переменные, изменение состояния
  //* endof вычисляемые переменные, изменение состояния

  //* секция эффекты
  React.useEffect(()=>{
    setShowPassword(showPasswordByDefault);
  }, [showPasswordByDefault])
  //* endof эффекты

  //* секция функции-хелперы, НЕ ОБРАБОТЧИКИ
  //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ

  //* секция обработчики
  //* endof обработчики

  return (
    <motion.div
      whileHover={{ scale: 1 + 0.02 * scalingMultiplier }}
      whileFocus={{ scale: 1 + 0.02 * scalingMultiplier }}
      whileTap={{ scale: 1 - 0.02 * scalingMultiplier }}
      transition={{ duration: 0 }}
      className={`w-100 position-relative ${wrapperClassName}`}
    >
      <input
        value={value}
        type={showPassword ? "text" : "password"}
        className={`form-control pl-10 ${className}`}
        {...otherProps}
      />
      <div
        className="text-badge bg-green show-password"
        onClick={(e) => setShowPassword(true)}
      >
        Показать
      </div>
      <div
        className="text-badge bg-green hide-password"
        onClick={(e) => setShowPassword(false)}
      >
        Скрыть
      </div>
    </motion.div>
  );
}
