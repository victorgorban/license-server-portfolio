import type { Meta, StoryObj } from "@storybook/react";

import Component, { ComponentProps } from "./MaskedInput";

const meta = {
  component: Component,
  title: "components/forms/MaskedInput",
  
  decorators: [
    (Story) => (
      <div className="content form d-flex flex-wrap pt-30 pb-40">
        <div className="input-wrapper">
          {/* 👇 Decorators in Storybook also accept a function. Replace <Story/> with Story() to enable it  */}
          <Story />
        </div>
      </div>
    ),
  ],
} satisfies Meta<typeof Component>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
  args: {
    value: "",
    mask: "+7(999)999-99-99",
  } as ComponentProps,
};

export const Filled: Story = {
  parameters: {
    docs: {
      description: {
        story: "Со значением",
      },
    },
  },
  args: {
    ...Primary.args, value: '9491243709'
  } as ComponentProps,
};
