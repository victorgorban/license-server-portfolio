"use client";
//* секция Библиотеки c функциями
import React from "react";
import AsyncSelect from "react-select/async";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
//* endof  Наши компоненты
export type ComponentProps = {
  /** Значение поля */
  value?: Record<string, any>;
  /** Колбек для открытия или закрытия меню */
  onMenuOpenOrClosed?: (isOpen: boolean) => void;
  /** Отключен или нет */
  isDisabled?: boolean;
  // TODO Настроить interactions. Нужно дать выбрать вариант обработчика хотя бы, а лучше дать его написать. 
  // TODO В конце-концов, для FormBlock+Select нужна функция. И ref. Нужно разобраться с этим прямо в документации - в конце-концов, это же тоже компонент.
  /** Доступен поиск по опциям или нет. Имеет смысл только при большом числе опций */
  isSearchable?: boolean;
  /** Опции, отображаемые по умолчанию, то есть до фильтрации поиском */
  defaultOptions: Record<string, any>[];
  /** Дополнительные классы */
  className?: string;
  /** Текст, отображаемый при закрытом меню, когда не выбрана ни одна опция */
  placeholder?: string;
  /** Другие свойства html-элемента */
  [key: string]: any;
};

/**
 * Select - компонент для форм
 * @returns TSX Компонент
 */
export default function SelectInput({
  value,
  onMenuOpenOrClosed = null,
  isDisabled = false,
  isSearchable = false,
  defaultOptions,
  className = "",
  placeholder = "Выбрать...",
  ...otherProps
}: ComponentProps): React.JSX.Element {
  //* секция глобальное состояние из context
  //* endof глобальное состояние из context

  //* секция состояние

  //* endof состояние

  //* секция вычисляемые переменные, изменение состояния

  //* endof вычисляемые переменные, изменение состояния

  //* секция эффекты

  //* endof эффекты

  //* секция функции-хелперы, НЕ ОБРАБОТЧИКИ

  //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ

  //* секция обработчики
  function handleMenuOpen() {
    // console.log('in handleMenuOpen')
    onMenuOpenOrClosed?.(true);
  }

  function handleMenuClose() {
    // console.log('in handleMenuClose')
    onMenuOpenOrClosed?.(false);
  }
  //* endof обработчики

  return (
    <AsyncSelect
      noOptionsMessage={() => "Ничего не найдено..."}
      className={`w-100 ${className}`}
      classNamePrefix="react-select"
      styles={{
        menu: (base) => ({ ...base, zIndex: 999 }),
        // menu: base => ({ ...base, zIndex: 9999 }),
      }}
      // menuPortalTarget={document.body}
      // document.body в модалке плохо работает. Можно не указывать, и поставить menuPosition="fixed", так работает
      menuShouldScrollIntoView={true}
      // menuPlacement="auto"
      // menuPosition={"fixed"}
      placeholder={placeholder}
      onMenuOpen={handleMenuOpen}
      onMenuClose={handleMenuClose}
      value={value}
      isDisabled={isDisabled}
      isSearchable={isSearchable}
      defaultOptions={defaultOptions}
      {...otherProps}
    />
  );
}
