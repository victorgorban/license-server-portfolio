import type { Meta, StoryObj } from "@storybook/react";

import Component, { ComponentProps } from "./SelectInput";

const meta = {
  component: Component,
  title: "components/forms/SelectInput",

  decorators: [
    (Story) => (
      <div
        className="content form d-flex flex-wrap pt-30 pb-40 overflow-visible"
        style={{ minHeight: 200 }}
      >
        <div className="input-wrapper" style={{ minWidth: 200 }}>
          {/* 👇 Decorators in Storybook also accept a function. Replace <Story/> with Story() to enable it  */}
          <Story />
        </div>
      </div>
    ),
  ],
} satisfies Meta<typeof Component>;

export default meta;
type Story = StoryObj<typeof meta>;

const optionsForSelect = [
  { value: 1, label: "Один" },
  { value: 2, label: "Два" },
];

const selectedOption = optionsForSelect[0];

export const Primary: Story = {
  args: {
    value: null,

    defaultOptions: optionsForSelect,
    loadOptions(inputValue, callback) {
      callback(
        optionsForSelect
          .filter((x) =>
            x.label.toLowerCase().includes(inputValue.toLowerCase())
          )
          .slice(0, 50)
      );
    },
  } as ComponentProps,
};

export const Filled: Story = {
  parameters: {
    docs: {
      description: {
        story: "Со значением",
      },
    },
  },
  args: {
    ...Primary.args,
    value: selectedOption,
  } as ComponentProps,
};

export const FilledDisabled: Story = {
  parameters: {
    docs: {
      description: {
        story: "Со значением, но отключен",
      },
    },
  },
  args: {
    ...Filled.args,
    isDisabled: true,
  } as ComponentProps,
};
