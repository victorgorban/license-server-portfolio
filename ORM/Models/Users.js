import _ from "lodash";
import Template from "./mongoTemplate";
import SimpleSchema from "simpl-schema";
let { Any, Integer, oneOf } = SimpleSchema;
import * as serverHelpers from "@serverHelpers";
import * as commonHelpers from "@commonHelpers";

export let collectionSchema = new SimpleSchema(
  {
    _id: String,
    createdAt: Date,
    updatedAt: Date,
    companyId: String,
    // для админов организации, email по умолчанию совпадает с email организации. 
    email: String,
    phone: String,
    name: String,
    username: { type: String, required: true },
    // пароль сохраняется в виде array-like объекта
    password: { type: Buffer, blackbox: true },
    loginTokens: [String],
    role: {
      type: String,
      allowedValues: commonHelpers.users.rolesForSelect.map(obj => obj.value),
    },
    isArchived: Boolean,
    archivedAt: Date
    // при создании компании хорошо бы сразу создавать админа компании (организация-юзер), с email хотя бы. Редактировать пользователей может только глобальный админ
  },
  serverHelpers.simpleSchemaOptions
);

export default class CollectionClass extends Template {
  static collectionName = "users";

  static indexes = [
    {
      definition: { username: 1 },
      options: {
        unique: true,
        sparse: true,
      },
    },
    {
      definition: { email: 1 },
      options: {
        unique: true,
        sparse: true,
      },
    },
  ];

  static schema = collectionSchema;

  static async afterUpdateOne(updatedDoc, updateQuery) {
    console.log('users afterUpdateOne', updatedDoc, updateQuery)
    if (_.has(updateQuery, '$set.email') || _.has(updateQuery, '$set.phone') || _.has(updateQuery, '$set.name')) {
      let name = _.get(updateQuery, '$set.name')
      let email = _.get(updateQuery, '$set.email')
      let phone = _.get(updateQuery, '$set.phone')
      await global.db.collection('licenseKeys').updateMany({ userId: updatedDoc._id }, { $set: { userData: { name, email, phone } } })
    }
  }
}
