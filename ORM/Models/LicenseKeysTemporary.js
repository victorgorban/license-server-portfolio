import _ from "lodash";
import Template from "./mongoTemplate";
import SimpleSchema, { Any, Integer, oneOf } from "simpl-schema";
import * as serverHelpers from "@serverHelpers";
import * as commonHelpers from "@commonHelpers";

export let collectionSchema = null


export default class CollectionClass extends Template {
  static collectionName = "licenseKeysTemporary";

  static indexes = [
    {
      definition: { createdAt: 1 },
      options: {
        expireAfterSeconds: 604_800 // 7 дней
      },
    },
  ];

  static schema = collectionSchema;
}
