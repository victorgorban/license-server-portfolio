import _ from "lodash";
import dayjs from "dayjs";
import Template from "./mongoTemplate";
import SimpleSchema, { Any, Integer, oneOf } from "simpl-schema";
import * as serverHelpers from "@serverHelpers";

export let collectionSchema = new SimpleSchema(
  {
    _id: String,
    createdAt: Date,
    updatedAt: Date,
    collectionName: String,
    method: String,
    comment: String, // для бэкапа это 'backup', остальное не знаю.
    findQuery: { type: Object, blackbox: true },
    actionQuery: { type: Object, blackbox: true },
    stateBefore: { type: Object, blackbox: true },
  },
  serverHelpers.simpleSchemaOptions
);


export default class CollectionClass extends Template {
  static collectionName = "oplog";

  static indexes = [
    {
      definition: { findQuery: 1 },
      options: {
      },
    }
  ];

  static schema = collectionSchema;

  static async afterInsertOne(doc) {
    console.log('oplog', !!doc, doc?.collectionName, doc?.method)
    // если это обновление связано с обновлением объекта, причем по _id, то глянуть его историю. 
    // Если пора делать бэкап (т.е. если последнее изменение было месяц и более назад, или менее месяца назад но изменений много (часто меняется, >5 за месяц)),
    // то делаем полный бэкап в дополнение к той истории что уже есть.
    // искать надо ДО ближайшего бэкапа. То есть берем историю за месяц, и уже потом обрезаем тех что был раньше ближайшего бэкапа.
    
  }
}
