//* Что это за файл? Здесь работа с MongoDB через ORM. Mongoose не подошел из-за неудобства в некоторых сценариях, плюс из-за общего снижения производительности.
//* Здесь же объединена скорость нативного драйвера с необходимой в наших проектах гибкостью.

import _ from "lodash";
import * as commonHelpers from "@commonHelpers";
import * as serverHelpers from "@serverHelpers";

global.DBCollections = global.DBCollections || {};

export default class Template {
  /**
   * object schema to check on insert/update
   */
  static schema = null;

  /**
   * Boolean, whether to use oplog on insert, update, delete, and indexes.
   */
  static mustSkipOplog = false;

  /**
   * Timestamps - это поля updatedAt и createdAt, также возможно deletedAt
   */
  static mustSkipTimestamps = false;

  static generateDocumentId = commonHelpers.randomString;

  /**
   * Список collectionindexes для создания. Почему я ушел в плане индексов от mongoose: mongoose не позволяет декларативно создавать сложные индексы
   * [{definition: {}, options: {}}]
   */
  static indexes = []

  /**
   * опции для создания коллекции. Например, {collation: {locale: 'en', strength: 2}} - таким образом _id будет case-insensitive.
   */
  static collectionOptions = {}

  /**
   * Название коллекции в db mongo
   */
  // в классе: static collectionName = "";
  static collectionName = "";

  static get collection() {
    if (!global.DBCollections[this.collectionName]) {
      throw new Error(`No collection named ${this.collectionName}`)
    }
    // пришлось сделать через функцию, т.к. переменная внутри класса оказывается новее этой же переменной, но вне класса. Возможно, это из-за статического импорта.
    return global.DBCollections[this.collectionName];
  }
  static set collection(newValue) {
    global.DBCollections[this.collectionName] = newValue;
  }

  /**
   * @unused
   */
  static helpers = {};

  /**
   * find a document matching the query.
   * @returns - then-able cursor, containing the found document or null
   * @param {Object} query - filter query
   * @param {Object} projection - filter query
   */
  static findOne(query, projection) {
    try {
      if (!query) query = {};
      if (typeof query != "object") {
        query = { _id: query };
      }

      return this.collection.findOne(query, projection);
    } catch (e) {
      throw e;
    }
  }

  /**
   * find documents matching the query.
   * @returns - then-able cursor, containing documents
   * @param {Object} query - filter query
   * @param {Object} projection - filter query
   */
  static find(query, projection) {
    try {
      if (Array.isArray(query)) {
        query = { _id: { $in: query } };
      }

      let cursor = this.collection.find(query, projection);

      return cursor;
    } catch (e) {
      throw e;
    }
  }

  /**
   * get count of documents matching the query.
   * @returns - then-able cursor, containing documents count
   * @param {Object} query - filter query
   */
  static async count(query) {
    try {
      await global.getDBInitialized();

      return this.collection.countDocuments(query);
    } catch (e) {
      throw e;
    }
  }

  /**
   * create collection index. If exists, replace it.
   * @returns {Promise<Object>} then-able cursor, creation info
   * @param {Object} definition - index definition, like { category: 1 }
   * @param {Object} options - index options, like { unique: true, sparse: true }
   * @param {Object} options.collation - index collation, like { locale: "en", strength: 2 } } - case-insensitive index.
   * @comment по умолчанию коллекция создается с collation={locale: 'simple'}, и все find по умолчанию используют collation коллекции.
   * @comment более того, индексы _id требует то же collation что и коллекция, поэтому для case-insensitive поиска _id нужна коллекция с соотв. collation.
   */
  static async createIndex(definition, options) {
    try {
      let result = await this.collection.createIndex(definition, options);
      return result;
    } catch (e) {
      throw e;
    }
  }

  /**
   * drops specified index
   * @returns {Promise<Object>} then-able cursor, index drop info
   * @param {string|Object} options - index name or index specification document like { "cat" : -1 }
   */
  static async removeIndex(index) {
    try {
      let result = await this.collection.dropIndex(index);

      return result;
    } catch (e) {
      throw e;
    }
  }


  /**
   * insert one document
   * @returns {Promise<Object>} insert info
   * @param {Object} document - document to insert
   */
  static async insertOne(document, validate = true) {
    try {
      await global.getDBInitialized();
      if (Array.isArray(document)) {
        throw new Error("Ожидается один документ для вставки");
      }

      if (!document._id) {
        document._id = this.generateDocumentId();
      }
      if (!this.mustSkipTimestamps) {
        if (!document.createdAt) {
          document.createdAt = new Date();
        }
        if (!document.updatedAt) {
          document.updatedAt = new Date();
        }
      }

      if (validate && this.schema) {
        this.schema.clean(document, { mutate: true });
        this.schema.validate(document);
      }

      let result = await this.collection.insertOne(document);

      await this.afterInsertOne?.(document);
      // если вставляется один документ как объект, то возвращается документ, а не массив.
      return document || null;
    } catch (e) {
      throw e;
    }
  }

  /**
   * insert many documents
   * @returns {Promise<Object>} insert info
   * @param {Array<Object>} documents - document to insert
   */
  static async insertMany(documents, validate = true) {
    try {
      await global.getDBInitialized();

      if (!Array.isArray(documents)) {
        throw new Error("Ожидается массив документов для вставки");
      }

      for (let document of documents) {
        if (!document._id) {
          document._id = this.generateDocumentId();
        }
        if (!this.mustSkipTimestamps) {
          if (!document.createdAt) {
            document.createdAt = new Date();
          }
          if (!document.updatedAt) {
            document.updatedAt = new Date();
          }
        }
      }

      if (validate && this.schema) {
        this.schema.clean(documents, { mutate: true });
        this.schema.validate(documents);
      }

      let result = await this.collection.insertMany(documents);

      return documents;
    } catch (e) {
      throw e;
    }
  }

  /**
   * update one document (options.multi is false)
   * @returns {Promise<Object>} update info
   * @param {Object} query - find query
   * @param {Object|Array<Object>} updateQuery - list of update operations in form of Object or Array of objects
   * @param {Object} [options={}] - options
   */
  static async updateOne(
    query,
    updateQuery,
    options = {},
    validate = true
  ) {
    try {
      await global.getDBInitialized();
      options.multi = false;

      if (!query) query = {};
      if (typeof query != "object") {
        query = { _id: query };
      }

      if (!this.mustSkipTimestamps) {
        if (!_.get(updateQuery, "$set.updatedAt")) {
          _.set(updateQuery, "$set.updatedAt", new Date());
        }
      }

      if (validate && this.schema) {
        this.schema.clean(updateQuery, { mutate: true, isModifier: true });
        this.schema.validate(updateQuery, { modifier: true });
      }

      let stateBefore = await this.collection.findOne(query);

      options.returnDocument = 'after';
      let result = await this.collection.findOneAndUpdate(query, updateQuery, options);

      await this.afterUpdateOne?.(result.value, updateQuery, stateBefore)

      return result.value;
    } catch (e) {
      throw e;
    }
  }

  static async replaceOne(
    query,
    replaceObject,
    options = {},
  ) {
    try {
      await global.getDBInitialized();

      if (!query) query = {};
      if (typeof query != "object") {
        query = { _id: query };
      }

      let stateBefore = await this.collection.findOne(query);

      let result = await this.collection.replaceOne(query, replaceObject, options);

      await this.afterReplaceOne?.(replaceObject, stateBefore)

      return result.value;
    } catch (e) {
      throw e;
    }
  }

  /**
   * update many documents
   * @returns {Promise<Object>} update info
   * @param {Object} query - find query
   * @param {Object|Array<Object>} updateQuery - list of update operations in form of Object or Array of objects (aggregation).
   * @param {Object} [options={}] - options
   */
  static async updateMany(
    query,
    updateQuery,
    options = {},
    validate = true
  ) {
    try {
      await global.getDBInitialized();
      options.multi = true;

      if (!query) query = {};
      if (Array.isArray(query)) {
        query = { _id: { $in: query } };
      }


      if (!this.mustSkipTimestamps) {
        if (!_.get(updateQuery, "$set.updatedAt")) {
          _.set(updateQuery, "$set.updatedAt", new Date());
        }
      }

      if (validate && this.schema) {
        this.schema.clean(updateQuery, { mutate: true, isModifier: true });
        this.schema.validate(updateQuery, { modifier: true });
      }

      let result = await this.collection.updateMany(
        query,
        updateQuery,
        options
      );

      return result;
    } catch (e) {
      throw e;
    }
  }

  /**
   * archives one document
   * @returns {Promise<Object>} archived doc
   * @param {Object} query - find query
   * @param {Object} [options={}] - options
   */
  static async archiveOne(query, isArchive, options = {}) {
    try {
      await global.getDBInitialized();
      options.multi = false;

      if (!query) query = {};
      if (typeof query != "object") {
        query = { _id: query };
      }

      let updateSet = { isArchived: true, archivedAt: new Date() }
      if (!isArchive) {
        updateSet = { isArchived: false, archivedAt: null }
      }

      let stateBefore = await this.collection.findOne(query);

      options.returnDocument = 'after';
      let updateResult = await this.collection.findOneAndUpdate(query, { $set: updateSet }, options);
      await this.afterArchiveOne?.(updateResult.value, { $set: updateSet }, stateBefore)

      return updateResult.value;
    } catch (e) {
      throw e;
    }
  }

  /**
   * deletes one document
   * @returns {Promise<Object>} delete info
   * @param {Object} query - find query
   * @param {Object} [options={}] - options
   */
  static async deleteOne(query, options = {}) {
    try {
      await global.getDBInitialized();
      options.multi = false;

      if (!query) query = {};
      if (typeof query != "object") {
        query = { _id: query };
      }


      let deletedDoc = await this.collection.findOneAndDelete(query, options);
      await this.afterDeleteOne?.(deletedDoc)

      return deletedDoc;
    } catch (e) {
      throw e;
    }
  }

  /**
   * archives many documents
   * @returns {Promise<Object>} update info
   * @param {Object} query - find query
   * @param {Object} [options={}] - options
   */
  static async archiveMany(query, isArchive = true, options = {}) {
    try {
      await global.getDBInitialized();
      options.multi = true;

      if (!query) query = {};
      if (Array.isArray(query)) {
        query = { _id: { $in: query } };
      }

      let updateSet = { isArchived: true, archivedAt: new Date() }
      if (!isArchive) {
        updateSet = { isArchived: false, archivedAt: null }
      }

      let result = await this.collection.updateMany(query, { $set: updateSet }, options);

      return result;
    } catch (e) {
      throw e;
    }
  }

  /**
   * delete many documents
   * @returns {Promise<Object>} delete info
   * @param {Object} query - find query
   * @param {Object} [options={}] - options
   */
  static async deleteMany(query, options = {}) {
    try {
      await global.getDBInitialized();
      options.multi = true;

      if (!query) query = {};
      if (Array.isArray(query)) {
        query = { _id: { $in: query } };
      }

      let result = await this.collection.deleteMany(query, options);

      return result;
    } catch (e) {
      throw e;
    }
  }

  static async afterInsertOne(doc) {
    return null;
  }

  static async afterUpdateOne(doc, updateQuery, stateBefore) {
    return null;
  }

  static async afterArchiveOne(doc, updateQuery, stateBefore) {
    return null;
  }

  static async afterDeleteOne(doc) {
    return null;
  }

  static async afterReplaceOne(doc) {
    return null;
  }
}
